#pragma once

#include "image.h"
#include "rw_status.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

enum read_status from_bmp(const FILE *in, struct image **img);
enum write_status to_bmp(const FILE *f, struct image *source);
