#pragma once

#include "stdbool.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

bool file_exists(const char *filename);
enum read_status open_image_rb(const char *filename, FILE **f);
enum write_status open_image_wb(const char *filename, FILE **f);
int close_img(FILE *f);
