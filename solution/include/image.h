#pragma once

#include "pixel.h"
#include "rw_status.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct image {
  uint64_t width;
  uint64_t height;
  struct pixel *data;
};

uint8_t calculate_padding(uint32_t width);
enum read_status read_image(FILE* f, const uint32_t width, const uint32_t height, struct image** img);
enum write_status write_image(FILE *f, struct image const *image, uint8_t padding);
