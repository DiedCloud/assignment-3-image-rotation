#pragma once

#include <stdint.h>

struct pixel {
  uint8_t r;
  uint8_t g;
  uint8_t b;
};
