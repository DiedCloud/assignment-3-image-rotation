#pragma once

#include "image.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

int rotate(struct image *source, int64_t angle);
