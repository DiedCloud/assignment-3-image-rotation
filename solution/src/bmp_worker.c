#include "bmp_header.h"
#include "image.h"
#include "rw_status.h"
#include "util.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct bmp_header create_header(const struct image *source,
                                const uint8_t padding) {
  struct bmp_header header = {
      header.bfType = SIGNATURE,
      header.bfileSize =
          sizeof(struct bmp_header) +
          sizeof(struct pixel) * (source->width + padding) * source->height,
      header.bfReserved = RESERVED,
      header.bOffBits = OFF_BITS,
      header.biSize = SIZE,
      header.biWidth = source->width,
      header.biHeight = source->height,
      header.biPlanes = PLANES,
      header.biBitCount = BIT_COUNT,
      header.biCompression = COMPRESSION_TYPE,
      header.biSizeImage = (source->width + padding) * source->height,
      header.biXPelsPerMeter = X_PELS_PER_METER,
      header.biYPelsPerMeter = Y_PELS_PER_METER,
      header.biClrUsed = CLR_USED,
      header.biClrImportant = CLR_IMPORTANT};
  return header;
}

enum read_status read_header(FILE *f, struct bmp_header *header) {
  if (fread(header, sizeof(struct bmp_header), 1, f) != 1) {
    log_err("READ INVALID HEADER");
    return READ_INVALID_HEADER;
  }
  if (header->bfType != SIGNATURE) {
    log_err("READ INVALID SIGNATURE");
    return READ_INVALID_SIGNATURE;
  }
  if (header->biBitCount != BIT_COUNT) {
    log_err("READ INVALID BITS");
    return READ_INVALID_BITS;
  }
  if (header->biCompression != COMPRESSION_TYPE) {
    log_err("READ INVALID COMPRESSION");
    return READ_INVALID_COMPRESSION;
  }

  return READ_OK;
}

enum read_status from_bmp(FILE *f, struct image **img) {
  struct bmp_header header;
  enum read_status read_header_status = read_header(f, &header);
  if (read_header_status == READ_OK) {
    return read_image(f, header.biWidth, header.biHeight, img);
  }
  return read_header_status;
}

enum write_status put_header(FILE *f, struct bmp_header *header) {
  if (fwrite(header, sizeof(struct bmp_header), 1, f) != 1) {
    log_err("Error writing header");
    return WRITE_ERROR;
  }

  return WRITE_OK;
}

enum write_status to_bmp(FILE *f, struct image *source) {
  uint8_t padding = calculate_padding((uint32_t)source->width);
  struct bmp_header header = create_header(source, padding);

  if (put_header(f, &header) != WRITE_OK) {
    return WRITE_ERROR;
  }

  return write_image(f, source, padding);
}
