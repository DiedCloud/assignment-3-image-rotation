#include "rw_status.h"
#include "stdbool.h"
#include "util.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


bool file_exists(const char *filename) { return access(filename, F_OK) == 0; }

enum read_status open_image_rb(const char *filename, FILE **f) {
  *f = fopen(filename, "rb");
  if (!f) {
    log_err("Error while opening file to read");
    return READ_OPEN_ERROR;
  }
  return READ_OK;
}

enum write_status open_image_wb(const char *filename, FILE **f) {
  *f = fopen(filename, "wb");
  if (f == NULL) {
    log_err("Error while opening file to write");
    return WRITE_OPEN_ERROR;
  }
  return WRITE_OK;
}

int close_img(FILE *f) {
  if (fclose(f) != 0) {
    log_err("Error while closing file");
    return 1;
  }
  return 0;
}
