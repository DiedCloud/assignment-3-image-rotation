#include "pixel.h"
#include "rw_status.h"
#include "util.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define C_PADDING 4

struct image {
  uint64_t width;
  uint64_t height;
  struct pixel *data;
};

struct pixel *generate_empty_pixels(struct pixel *storage, uint32_t width,
                                    uint32_t height) {
  for (uint32_t i = 0; i < (height * width); ++i) {
    storage[i].b = 0;
    storage[i].g = 0;
    storage[i].r = 0;
  }
  return storage;
}

struct image *create_blank_image(uint32_t width, uint32_t height) {
  struct image *empty_image = (struct image *)malloc(sizeof(struct image));
  if (empty_image) {
    empty_image->width = width;
    empty_image->height = height;
    empty_image->data =
        (struct pixel*)malloc(height * width * sizeof(struct pixel));
    if (empty_image->data) {
      empty_image->data = generate_empty_pixels(empty_image->data, width, height);
    }
    else {
      free(empty_image);
      return NULL;
    }
  }
  return empty_image;
}

uint8_t calculate_padding(uint32_t width) {
  return (C_PADDING - width * sizeof(struct pixel) % C_PADDING) % C_PADDING;
}

enum read_status read_image(FILE *f, const uint32_t width, const uint32_t height, struct image** img) {
  uint8_t padding = calculate_padding(width);

  *img = create_blank_image(width, height);

  if (*img) {
    for (uint32_t i = 0; i < height; ++i) {
      if (fread(&(*img)->data[i * width], sizeof(struct pixel), width, f) != width || fseek(f, padding, SEEK_CUR)) {
        free((*img)->data);
        free(*img);
        return READ_ERROR;
      }
    }
    return READ_OK;
  }
  return ALLOCATION_ERROR;
}

enum write_status write_image(FILE *f, struct image const *image,
                              uint8_t padding) {
  uint8_t padding_array[3] = {0};
  for (uint32_t i = 0; i < image->height; i++) {
    void* buff = &image->data[i * image->width];
    if (fwrite(buff, sizeof(struct pixel),
               image->width, f) != image->width) {
      log_err("Error writing data");
      return WRITE_ERROR;
    }

    if (padding > 0) {
      if (fwrite(padding_array, 1, padding, f) != padding) {
        log_err("Error writing padding");
        return WRITE_ERROR;
      }
    }
  }

  return WRITE_OK;
}
