﻿#include "bmp_worker.h"
#include "file_worker.h"
#include "rotate.h"
#include "rw_status.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "image.h"
#include "util.h"


int main(int argc, char **argv) {
  if (argc != 4) {
    printf("ERROR: Invalid number of arguments, expected 3, got %d", argc - 1);
    return 1;
  }
  if (!file_exists(argv[1])) {
    log_err("ERROR: Invalid filename");
    return 1;
  }

  const char *source_filename = argv[1];
  const char *destination_filename = argv[2];
  int16_t angle = (int16_t)atoi(argv[3]);

  FILE *fr;
  if (open_image_rb(source_filename, &fr) != READ_OK) {
    if (close_img(fr)) {
      return 2;
    }
    return 1;
  }
  struct image *image;
  if (from_bmp(fr, &image) != READ_OK) {
    if (close_img(fr)) {
      return 2;
    }
    return 1;
  }
  if (close_img(fr)) {
    free(image->data);
    free(image);
    return 2;
  }

  if (rotate(image, angle)) {
      log_err("ERROR: Rotating error");
  }

  FILE *fw;
  if (open_image_wb(destination_filename, &fw) != WRITE_OK) {
    free(image->data);
    free(image);
    if (close_img(fr)) {
      return 2;
    }
    return 1;
  }
  if (to_bmp(fw, image) != WRITE_OK) {
    free(image->data);
    free(image);
    if (close_img(fr)) {
      return 2;
    }
    return 1;
  }
  if (close_img(fw)) {
    free(image->data);
    free(image);
    return 2;
  }

  free(image->data);
  free(image);
  return 0;
}
