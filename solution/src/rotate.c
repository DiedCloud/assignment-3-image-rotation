#include "pixel.h"
#include "image.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

int static rotate270(struct image *source) {
  struct pixel *rotated_data = (struct pixel*)
      malloc(source->height * source->width * sizeof(struct pixel));
  if (!rotated_data) {
      return 1;
  }
  struct pixel *source_data = source->data;

  uint64_t res_width = source->height;
  uint64_t res_height = source->width;

  int32_t move_for;
  size_t start;

  for (size_t i = 0; i < source->height; i++) {
    for (size_t j = 0; j < source->width; j++) {
      move_for = (int32_t)
          ((source->height - 1) * (j + 1) - (source->width + 1) * i);
      start = i * (source->width) + j;
      rotated_data[start + move_for] = source_data[start];
    }
  }

  free(source->data);
  source->data = rotated_data;
  source->width = res_width;
  source->height = res_height;
  return 0;
}

int static rotate180(struct image *source) {
  struct pixel *rotated_data = (struct pixel*)
      malloc(source->height * source->width * sizeof(struct pixel));
  if (!rotated_data) {
      return 1;
  }
  struct pixel *source_data = source->data;

  size_t start;

  for (size_t i = 0; i < source->height; i++) {
    for (size_t j = 0; j < source->width; j++) {
      start = i * source->width + j;
      rotated_data[source->height * source->width - start - 1] =
          source_data[start];
    }
  }
  free(source->data);
  source->data = rotated_data;
  return 0;
}

int static rotate90(struct image *source) {
  struct pixel *rotated_data = (struct pixel*)
      malloc(source->height * source->width * sizeof(struct pixel));
  if (!rotated_data) {
      return 1;
  }
  struct pixel *source_data = source->data;

  uint64_t res_width = source->height;
  uint64_t res_height = source->width;

  size_t offset = source->height * source->width - source->height;

  int32_t move_for;
  size_t start;

  for (size_t i = 0; i < source->height; i++) {
    for (size_t j = 0; j < source->width; j++) {
      move_for = (int32_t)
          (offset - (source->height + 1) * j - (source->width - 1) * i);
      start = i * source->width + j;
      rotated_data[start + move_for] = source_data[start];
    }
  }
  free(source->data);
  source->data = rotated_data;
  source->width = res_width;
  source->height = res_height;
  return 0;
}

int rotate(struct image* source, int64_t angle) {
    while (angle < 0) {
        angle += 360;
    }
    while (angle > 360) {
        angle -= 360;
    }

    switch (angle) {
    case 0:
        return 0;
    case 90:
        return rotate90(source);
    case 180:
        return rotate180(source);
    case 270:
        return rotate270(source);
    default:
        fprintf(
            stderr,
            "ERROR: Unsupported angle. Suppurts: -270; -180; -90; 0; 90; 180; 270");
        return 2;
    }
}
